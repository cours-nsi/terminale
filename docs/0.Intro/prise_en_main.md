---
author: à compléter
title: Prise en main
---

# Prise en main



## Organisation du cours
Le cours présenté sur ce site est divisé en cinq parties :

<ol>
    <li>Programmation</li>
    <li>Algorithmique</li>
    <li>Graphes et arbres</li>
    <li>Bases de données</li>
    <li>Architecture matérielle</li>
</ol>

Chacune de ces parties est présentée sur une page Web dédiée. On y trouve en particulier des références au <a href='https://eduscol.education.fr/document/30010/download' target="_blank">Bulletin Officiel</a>, ainsi que les différentes notions qui y seront abordées.

Chaque partie est ensuite découpée en plusieurs chapitres.

<span style='font-style:italic;'>Avertissement : le découpage en parties et chapitres qui est proposé ici n'est en aucun cas un découpage &laquo; officiel &raquo; et n'engage que les auteurs de ce cours. De plus, il n'est pas nécessaire de suivre ce cours de manière linéaire (ce que les auteurs ne font d'ailleurs pas dans leur cours...).</span>

## Organisation des chapitres
Afin d'avoir un ensemble homogène sur l'ensemble des pages du site, tous les chapitres du cours sont organisés de la même façon en plusieurs sections :

<ul>
    <li>une image illustrant le chapitre ;</li>
    <li>le portrait d'une femme informaticienne ;</li>
    <li>une section d'intentions concernant le déroulé du chapitre ;</li>
    <li>une section de cours dans laquelle on propose des TP, du cours ou des exercices en version pdf téléchargeables ;</li>
    <li>une section de ressources à télécharger, éventuellement vide.</li>
</ul>
Lorsque le chapitre nécessite l'utilisation de notebooks jupyter, ceux-ci peuvent être obtenus directement à l'aide de la bibliothèque partagée de <a href='https://capytale2.ac-paris.fr/web/c-auth/list' target="_blank">Capytale</a> ou être téléchargés directement dans la sous-section <span style='font-style:italic;'>Ressources</span>. Une fois téléchargés, ceux-ci sont alors visibles sur <a href="https://basthon.fr/" target="_blank"> Basthon</a>.<br>
Les ressources à télécharger peuvent aussi ne pas être des notebooks. Cela peut être des fichiers spécifiques d'exercices (comme par exemple sur les réseaux), ou des documents ressources.<br>
Dans tous les cas, si un lien de téléchargement est proposé, visitez le !

## Epreuves du bac et challenges

Le site regroupe également, au format pdf, l'intégralité des épreuves écrites et pratiques du baccalauréat. Des tableaux listant les thèmes abordés dans chaque sujet sont également proposés année par année.

Enfin, plusieurs challenges en algorithmique et en programmation Python/Javascript sont également proposés en guise d'approfondissement des notions abordées à travers ce cours.