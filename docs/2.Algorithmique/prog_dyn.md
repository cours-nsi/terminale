---
author: à compléter
title: Programmation dynamique
---

# Programmation dynamique



## Intentions

!!! abstract "Introduction"

    Il existe de nombreuses approches algorithmiques pour résoudre un problème donné en se basant sur la résolutions de ses sous-problèmes. En Première, nous avons parlé de l'_approche gloutonne_ et nous avons déjà vu cette année l'approche basée sur la méthode _&laquo; Diviser pour régner &raquo;_.
    
    Dans ce chapitre, nous abordons une nouvelle approche algorithmique : la _programmation dynamique_.

    Celle-ci consiste en une mémoïsation des résultats déjà calculés afin de ne pas avoir à recalculer plusieurs fois les mêmes données.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un notebook pour retravailler les approches gloutonnes et récursives ;
    2. un notebook traitant du rendu de monnaie pour introduire la programmation dynamique ;
    3. un point de cours interactif présentant deux applications classiques de la programmation dynamique : l'algorithme de Bellman-Ford et le problème de l'alignement de séquences ADN.

## Cours

??? note "Exercices (Toto le robot)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 2 Chapitre 3 Toto le robot
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Exercices (rendu de monnaie)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 2 Chapitre 3 Rendu monnaie
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/ProgrammationDynamique_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/2.Algorithmique/a_telecharger/chapitre_3/notebooks){ .md-button target="_blank" rel="noopener" }