---
author: à compléter
title: Méthode &laquo; Diviser pour régner &raquo;
---

# Méthode &laquo; Diviser pour régner &raquo;



## Intentions

!!! abstract "Introduction"

    La méthode &laquo; Diviser pour régner &raquo; est une méthode algorithmique qui consiste à décomposer un problème en sous-problèmes plus faciles à résoudre, puis à combiner leurs solutions afin de construire une solution au problème de départ.

    On présente divers exemples classiques d'applications, notamment le _tri fusion_ et la rotation d'images, et on s'intéresse à l'efficacité de cette méthode au regard d'autres approches (itératives, gloutonnes, etc.)


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. une présentation brève de la méthode &laquo; Diviser pour régner &raquo; ;
    2. un notebook d'entraînement traitant trois exemples classiques : la recherche dichotomique, la recherche du maximum dans un tableau non trié et le tri fusion ;
    3. un notebook d'entraînement traitant de la rotation d'images.

## Cours

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/DiviserPourRegner_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices (recherche dichotomique, recherche du maximum, tri fusion)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 2 Chapitre 2 Diviser pour régner
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Exercices (rotation d'images)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 2 Chapitre 2 Rotation image
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/2.Algorithmique/a_telecharger/chapitre_2/notebooks){ .md-button target="_blank" rel="noopener" }