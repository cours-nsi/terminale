---
author: à compléter
title: Recherche textuelle
---

# Recherche textuelle



## Intentions

!!! abstract "Introduction"

    Comment déterminer de façon efficace si une chaîne de caractères est présente ou non dans un texte donné ?
    
    Pour répondre à cette question classique (qui n'a jamais utilisé le raccourci CTRL+F dans un pdf ?), plusieurs algorithmes ont été inventés : algorithme de Knuth-Morris-Pratt, algorithme de Boyer-Moore, algorithme de Horspool, etc.

    Dans ce chapitre, nous présentons l'algorithme de Boyer-Moore, tout d'abord dans sa version &laquo; naïve &raquo;, puis dans sa version simplifée due à Horspool.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. une activité au format notebook pour travailler sur l'algorithme de Boyer-Moore ;
    2. ce notebook se découpant en trois parties distinctes, le professeur veillera à bien faire une synthèse de ce qui a été fait à l'issue de chacune de ces parties.

## Cours

??? note "Activité"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 2 Chapitre 4 Recherche textuelle
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous


## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/2.Algorithmique/a_telecharger/chapitre_4/notebook){ .md-button target="_blank" rel="noopener" }