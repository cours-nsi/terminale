---
author: à compléter
title: Structures de données linéaires
---

# Structures de données linéaires

![](../images/file.png){: .center }


!!! tip "Sarah Cohen-Boulakia"

    ![](../images/sarah.jpg){: .center }

    **Sarah Cohen-Boulakia** est bioinformaticienne,  professeure à l’Université Paris Saclay et chercheuse au Laboratoire Interdisciplinaire des Sciences du Numérique. Elle est spécialiste en science des données, notamment de l’analyse et l’intégration de données biologiques et biomédicales. Pendant la crise du covid, elle a participé à l’intégration des résultats de milliers d’essais cliniques. Elle a obtenu en 2024 la médaille d’argent du CNRS. Elle est directrice adjointe sur les aspects formation de l’institut DATAIA. Elle participe également au montage du réseau français de reproductibilité.


## Intentions

!!! abstract "Introduction"

    Les tableaux vus en Première étant des structures linéaires statiques, elles sont trop &laquo; rigides &raquo; pour pouvoir être utilisées de façon simple dans de nombreux problèmes.

    Dans ce chapitre, on aborde trois structures linéaires _dynamiques_ : les listes chaînées, les piles et les files.

    On présente leur interface, ainsi que diverses implémentations.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif pour présenter l'interface et diverses implémentations des listes chaînées ;
    2. un notebook d'entraînement pour manipuler les listes chaînées ;
    3. une activité, au format notebook, pour introduire les piles et les files
    4. un point de cours interactif pour présenter l'interface et diverses implémentations des piles et des files ;
    5. un notebook d'entraînement pour manipuler les piles et les files.

## Cours

??? note "Cours (listes chaînées)"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_1/ListesChainees_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices (listes chaînées)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 2 Chapitre 1 Listes
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Activité : les poupées russes"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 2 Chapitre 1 Poupées russes
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Cours (piles et files)"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_1/PilesFiles_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices (piles et files)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 2 Chapitre 1 Piles files
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/2.Algorithmique/a_telecharger/chapitre_1/notebooks){ .md-button target="_blank" rel="noopener" }