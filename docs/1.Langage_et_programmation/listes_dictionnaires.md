---
author: à compléter
title: Listes et dictionnaires
---

# Listes et dictionnaires



## Intentions

!!! abstract "Introduction"

    Les listes et les dictionnaires sont les deux structures de données les plus importantes introduites dans la classe de Première.

    Celles-ci seront utilisées énormément tout au long de la classe de Terminale pour implémenter divers objets abstraits (listes chaînées, piles, files, graphes et arbres).

    Ce premier chapitre est un chapitre de révision.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un notebook de révision sur les listes ;
    2. un notebook de révision sur les dictionnaires.

## Cours

??? note "Exercices (listes)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 1 Chapitre 1 Rappels listes
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Exercices (dictionnaires)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 1 Chapitre 1 Rappels dictionnaires
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous



## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/1.Langage_et_programmation/a_telecharger/chapitre_1/notebooks){ .md-button target="_blank" rel="noopener" }