---
author: à compléter
title: Introduction
---

!!! abstract "Présentation générale"

    Le travail entrepris en classe de Première sur les méthodes de programmation est prolongé. L’accent est mis sur une programmation assurant une meilleure sûreté, c’est-à-dire minimisant le nombre d’erreurs. Parallèlement, on montre l’universalité et les limites de la notion de calculabilité.

    La récursivité est une méthode fondamentale de programmation. Son introduction permet également de diversifier les algorithmes étudiés. En classe de Terminale, les élèves s’initient à différents paradigmes de programmation pour ne pas se limiter à une démarche impérative.





!!! info "Points abordés"

    1. Rappels sur les listes et les dictionnaires.

    2. Récursivité :
        - utilisation d'outils de visualisation graphique ;
        - notions de récursivité.

    3. Programmation objet :
        - notions de programmation objet ;
        - vocabulaire : classe, objet, attribut, méthode.

    4. Gestion des exceptions :
        - classification des exceptions ;
        - structure `try ... except ...` ;
        - mots-clés `assert`, `break`, `continue`, `pass`.

    5. Décidabilité et calculabilité :
        - comprendre qu'un programme est une donnée ;
        - comprendre que la calculabilité ne dépend pas du langage utilisé ;
        - problème de l'arrêt.

    6. Modularité et mise au point de programmes :
        - utilisation de bibliothèques ;
        - exploitation de documentations ;
        - création de modules simples.

    7. Programmation fonctionnelle :
        - distinguer sur des exemples les paradigmes impératif, fonctionnel et objet ;
        - choisir le paradigme de programmation selon le champ d’application d’un programme.


??? note "Document ressource"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_0/Commandes_principales_Python_NSI.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>