---
author: à compléter
title: Programmation objet
---

# Programmation objet



## Intentions

!!! abstract "Introduction"

    Depuis que nous manipulons le langage Python nous avons pu remarquer que lorsque l’on demande le type d’une donnée ou d’une
    variable on obtient une réponse contenant le mot-clé `class`. Par exemple l’instruction `type(5)` renvoie `<class 'int'>`.

    De même, quand nous voulons effectuer certaines opérations on peut provoquer une erreur dans laquelle Python utilise le mot
    `object`. Par exemple l’instruction `'2'+ 2` renvoie l’erreur `TypeError: Can't convert 'int' object to str implicitly`.

    De plus, quand nous avons manipulé en Première les types `list` ou `dict`, on a pu remarqué que des fonctions associées à ces types étaient prédéfinies et accessibles en faisant suivre le nom de la variable d’un point puis du nom de la fonction. Ces fonctions associées sont appelées des _méthodes_. Par exemple pour ajouter à la fin d’un tableau `L` (type `list`) un nouvel élément `e` on saisit l'instruction `L.append(e)`.

    Le langage Python est un langage _orienté objet_ dans lequel tous les types de base définis sont des classes. Chaque classe possède des caractéristiques et des fonctionnalités qui lui sont propres. Ainsi chaque donnée d’un type particulier est une instance de la classe associée. On nomme aussi ces instances de classe des objets.
    
    En clair en Python tout est objet et dans ce chapitre nous allons voir comment définir nos propres structures de données et ainsi apprendre à mieux structurer nos programmes.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif pour présenter la programmation objet pour introduire et manipuler les classes, les objets et les méthodes  ;
    2. un notebook d'entraînement pour manipuler les notions vues dans le cours.

## Cours

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/Objet_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 1 Chapitre 3 POO
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous


## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/1.Langage_et_programmation/a_telecharger/chapitre_3/notebook){ .md-button target="_blank" rel="noopener" }