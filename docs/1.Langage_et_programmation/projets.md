---
author: à compléter
title: Projets
---

# Projets



## Intentions

!!! abstract "Introduction"

    On propose ci-dessous divers projets de programmation Python au format notebook.


## Liste des projets

??? note "Pile de glaces"

    Projet guidé au format notebook pour manipuler à la fois la programmation objet et les piles.

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 1 Projet Pile de glaces
    ```

    - **Accès sans Capytale :** télécharger le projet à l'aide du lien ci-dessous

    [Téléchargement du projet _Pile de glaces_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/1.Langage_et_programmation/a_telecharger/chapitre_8/pile_de_glaces){ .md-button target="_blank" rel="noopener" }


??? note "Playlist"

    Projet guidé au format notebook pour travailler la programmation objet afin d'implémenter une playlist liée à un fichier csv.

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrasea ci-dessous :

    ```python
    NSI Terminale Partie 1 Projet MP3 Playlist
    ```

    - **Accès sans Capytale :** télécharger le projet à l'aide du lien ci-dessous

    [Téléchargement du projet _Playlist_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/1.Langage_et_programmation/a_telecharger/chapitre_8/playlist){ .md-button target="_blank" rel="noopener" }


 