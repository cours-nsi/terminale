---
author: à compléter
title: Récursivité
---

# Récursivité



## Intentions

!!! abstract "Introduction"

    On a vu jusqu’à présent que des fonctions pouvaient appeler d’autres fonctions. Il est également possible de programmer une fonction qui s’appelle elle-même. On parle alors de _fonction récursive_.
    
    La récursivité est une démarche qui fait référence à l’objet même de la démarche à un moment du processus. En d’autres termes, c’est une démarche dont la description mène à la répétition d’une même règle (Wikipédia).
    
    En informatique, un algorithme qui contient des appels à lui-même est dit récursif.

    Le but de ce chapitre est de présenter cette démarche et d'en dégager quelques concepts clés. On s'appuie en particulier sur deux applications de visualisation graphique : Python Tutor et Recursion Visualizer.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un notebook d'approfondissement sur les appels de fonctions ;
    2. un notebook d'introduction aux différentes notions clés de la récursivité ;
    3. un point de cours interactif pour résumer ce qui a été vu dans le notebook précédent ;
    4. un notebook d'entraînement (que l'on peut filer sur plusieurs semaines) pour manipuler les notions vues précédemment.

## Cours

??? note "Exercices (rappels sur les appels de fonctions)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 1 Chapitre 2 Récursivité (1-3) - notions de fonctions
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Exercices (introduction à la récursivité)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 1 Chapitre 2 Récursivité (2-3) - notions de récursivité
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/Recursivite_cours_complété.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices (applications de la récursivité)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 1 Chapitre 2 Récursivité (3-3) - mise en pratique
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous


## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/1.Langage_et_programmation/a_telecharger/chapitre_2/notebooks){ .md-button target="_blank" rel="noopener" }