---
author: à compléter
title: Programmation fonctionnelle
---

# Programmation fonctionnelle



## Intentions

!!! abstract "Introduction"

    Il existe de très nombreux langages de programmation, chacun ayant ses particularités. Ils sont en général liés à un _paradigme de programmation_ (façon de penser les problèmes et d’écrire des programmes pour les résoudre).
    
    Nous utilisons depuis la classe de Première principalement le modèle de programmation _impérative_ (on dit à l’ordinateur quoi faire à chaque étape, on utilise des variables, donc des affectations, l’exécution du programme est en fait une suite de modifications des états des variables) mais on a vu également les bases de la programmation _orientée objet_.

    Dans ce chapitre, nous nous initions à un autre paradigme de programmation : la _programmation fonctionnelle_.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif pour présenter ce qu'est la programmation fonctionnelle et quelques outils Python tournés vers ce paradigme ;
    2. des exercices d'applications de ce paradigme.

## Cours

??? note "Cours et applications"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_7/ProgFonctionnelle.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

_Il n'y a pas de ressources associées à ce chapitre._