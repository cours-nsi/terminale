---
author: à compléter
title: Calculabilité et décidabilité
---

# Calculabilité et décidabilité



## Intentions

!!! abstract "Introduction"

    Un ordinateur peut-il tout calculer ? Existe-t-il toujours un algorithme pour résoudre un problème? Le pouvoir de l’algorithmique est-il infini ?
    
    La réponse est bien évidemment non, mais la notion de _calculabilité_ permet de mieux définir les possibilités et limites de l’algorithmique.
    
    Une autre notion, fondamentalement liée à ces questions est la notion de _décidabilité_ qui sépare les problèmes dits &laquo; de décision &raquo; en deux classes : ceux auxquels on peut apporter une réponse et les autres.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours pour comprendre qu'un programme est aussi une donnée ;
    2. un notebook pour illustrer cette notion ;
    3. un point de cours interactif pour présenter les notions de calculabilité et de décidabilité.

## Cours

??? note "Cours (complet)"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_5/CalculabilitéDécidabilité_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices (un programme est une donnée)"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 1 Chapitre 5 Programme vs donnée
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous


## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/1.Langage_et_programmation/a_telecharger/chapitre_5/notebook){ .md-button target="_blank" rel="noopener" }