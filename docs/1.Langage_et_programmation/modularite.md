---
author: à compléter
title: Modularité et mise au point de programmes
---

# Modularité et mise au point de programmes



## Intentions

!!! abstract "Introduction"

    La documentation des fonctions et l'utilisation de bibliothèques permet de mieux structurer les programmes que l'on écrit.

    Le but de ce chapitre est multiple :

     - montrer comment créer et utiliser ses propres bibliothèques ;
     - utiliser un environnement de développement autre que les notebooks ;
     - manipuler une interface graphique ;
     - s'initier à la programmation événementielle ;
     - conduire un projet en groupe.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. des exercices de prise en main de la bibliothèque `graphics_nsi` basée sur le module `pygame` ;
    2. un mini-projet pas à pas pour concevoir un jeu avec interface graphique ;
    3. un projet à réaliser en groupe (une proposition de projets possibles est donnée).

## Cours

??? note "Exercices (prise en main de la bibliothèque graphique et mini-projet)"

    **La bibliothèque `graphics_nsi` et sa documentation sont à télécharger dans la rubrique Ressources ci-dessous.**

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_6/Modularite_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices (proposition de projets)"

    **Remarque : pour le projet 5, des fichiers annexes sont à télécharger dans la rubrique Ressources ci-dessous.**

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_6/Enonces_projets_programmation.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


## Ressources

[Téléchargement de la bibliothèque `graphics_nsi`](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/1.Langage_et_programmation/a_telecharger/chapitre_6/biblio){ .md-button target="_blank" rel="noopener" }

[Téléchargement des ressources pour les projets](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/1.Langage_et_programmation/a_telecharger/chapitre_6/projets){ .md-button target="_blank" rel="noopener" }