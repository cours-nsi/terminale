---
author: à compléter
title: Gestion des exceptions
---

# Gestion des exceptions



## Intentions

!!! abstract "Introduction"

    Lorsque l’on demande à Python de faire une action impossible, il lève une _exception_ et renvoie une erreur.

    Le but de ce chapitre est de comprendre comment anticiper ces exceptions, comment les intercepter et, le cas échéant, comment les contourner.


!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif pour présenter les différentes exceptions qui existent en Python, ainsi que les structures et mots-clés pour les contrôler ;
    2. ce point de cours se découpant naturellement en plusieurs parties, le professeur veillera bien à proposer divers exemples de manipulation.

## Cours

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_4/GestionDesExceptions.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

_Il n'y a pas de ressources associées à ce chapitre._