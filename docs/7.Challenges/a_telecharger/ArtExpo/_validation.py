from _validation import *
from capytale.autoeval import (
    ValidateVariables,
)

test_code_1 = ValidateVariables({"code1": [7,16,24]})
test_code_2 = ValidateVariables({"code2": [18,32,5]})
test_code_3 = ValidateVariables({"code3": [10, 22, 31]})
test_code_4 = ValidateVariables({"code4": [8, 16, 24]})
test_code_5 = ValidateVariables({"code5": [17, 9, 35]})
test_code_6 = ValidateVariables({"code6": [12, 23, 12]})
test_code_7 = ValidateVariables({"code7": [12, 27, 20]})
test_code_8 = ValidateVariables({"code8": [2, 1, 8]})
test_code_9 = ValidateVariables({"code9": [12, 24, 9]})
test_code_10 = ValidateVariables({"code10": [7, 4, 1]})
test_code_11 = ValidateVariables({"code11": [7, 5, 3]})
test_code_12 = ValidateVariables({"code12": [24, 15, 6]})