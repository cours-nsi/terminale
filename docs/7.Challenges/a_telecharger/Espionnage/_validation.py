from capytale.autoeval import Validate, validationclass
import sys


@validationclass 
class ValidationMessage(Validate):
    def __init__(self,val):
        super().__init__()
        self.val = val

    def __call__(self,message):
        if message == self.val:
            print('Bravo ! Vous avez découvert le bon lieu !')
            self._success()  # c'est cet appel qui affiche le reste du notebook
        else:
            print("Désolé ! ce n'est pas le bon lieu !", file=sys.stderr)

validation_lieu_rdv = ValidationMessage('barcelona_parque_de_las_cascadas')
validation_lieu_prison = ValidationMessage('lloret_de_mar_avinguda_america')