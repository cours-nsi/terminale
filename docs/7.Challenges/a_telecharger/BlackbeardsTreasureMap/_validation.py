from capytale.autoeval import Validate, validationclass
import sys

@validationclass 
class ValidationCoordinates(Validate):
    def __init__(self,val):
        super().__init__()
        self.val = val

    def __call__(self,coord):
        if coord == self.val:
            print("""The coordinates are correct!\n
You've just found Blackbeard's hidden treasures!""")
            self._success()  # c'est cet appel qui affiche le reste du notebook
        else:
            print("Sorry, the coordinates don't work!\nCarry on digging!", file=sys.stderr)



validate_coordinates = ValidationCoordinates((2463, 767))