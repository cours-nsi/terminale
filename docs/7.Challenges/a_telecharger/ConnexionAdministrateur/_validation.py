from capytale.autoeval import Validate, validationclass
import sys


@validationclass 
class ValidationMessage(Validate):
    def __init__(self,val):
        super().__init__()
        self.val = val

    def __call__(self,message):
        if message == self.val:
            print('Flag validé !')
            self._success()  # c'est cet appel qui affiche le reste du notebook
        else:
            print("Désolé ! ce n'est pas le bon flag !", file=sys.stderr)


validation_connexion1 = ValidationMessage('admin')
validation_connexion2 = ValidationMessage('4dm1nP@sSw0rD')
validation_connexion3 = ValidationMessage('@dM1n1sTrat1oNm0t_de_P@sS3')
validation_connexion4 = ValidationMessage('AdM1n_R3s3@u{0rD1n@t3uR_pr1nC1P@L}')
validation_connexion5 = ValidationMessage('admin@1Adm1n')