from capytale.autoeval import Validate, validationclass
import sys

T = {'1': 'golden coins',
     '2': 'a dark red ruby',
     '3': 'a sparkly diamond',
     '4': 'a rare crystal',
     '5': 'a gloden compass',
     '6': 'an anchor made of silver',
     '7': 'a treasure map',
     '8': "Blackbeard's sword",
     '9': 'a golden ring',
     '10': 'a barrel of rum',
     '11': 'a message in a bottle',
     '12': "Blackbeard's flag"}

@validationclass 
class ValidationCoordinates():
    def __init__(self,val,num):
        super().__init__()
        self.val = val
        self.num = num

    def __call__(self,coord):
        if coord == self.val:
            print("""The coordinates on parchment {0} are correct!\n
You've just found one of Blackbeard's hidden treasures: {1}!""".format(self.num,T[self.num]))
            self._success()  # c'est cet appel qui affiche le reste du notebook
        else:
            print("Sorry, the coordinates on parchment {0} don't work!\nCarry on digging!".format(self.num), file=sys.stderr)



test_parchment_1 = ValidationCoordinates('A3', '1')
test_parchment_2 = ValidationCoordinates('E6', '2')
test_parchment_3 = ValidationCoordinates('K6', '3')
test_parchment_4 = ValidationCoordinates('F9', '4')
test_parchment_5 = ValidationCoordinates('A6', '5')
test_parchment_6 = ValidationCoordinates('B9', '6')
test_parchment_7 = ValidationCoordinates('J4', '7')
test_parchment_8 = ValidationCoordinates('N4', '8')
test_parchment_9 = ValidationCoordinates('K5', '9')
test_parchment_10 = ValidationCoordinates('C2', '10')
test_parchment_11 = ValidationCoordinates('F7', '11')
test_parchment_12 = ValidationCoordinates('A1', '12')