from capytale.autoeval import Validate, validationclass
import sys


@validationclass 
class ValidationMessage(Validate):
    def __init__(self,val):
        super().__init__()
        self.val = val

    def __call__(self,message):
        if message == self.val:
            print('Flag décodé avec succès !')
            self._success()  # c'est cet appel qui affiche le reste du notebook
        else:
            print("Désolé ! ce n'est pas le bon flag !", file=sys.stderr)

validation_message = ValidationMessage('Easy')