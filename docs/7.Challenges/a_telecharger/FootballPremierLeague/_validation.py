from capytale.autoeval import Validate, validationclass
import sys

@validationclass 
class ValidationTask():
    def __init__(self,val):
        super().__init__()
        self.val = val

    def __call__(self):
        print('Task {0} completed'.format(self.val))
        self._success()  # c'est cet appel qui affiche le reste du notebook
        
validation_task_1 = ValidationTask(1)
validation_task_2 = ValidationTask(2)
validation_task_3 = ValidationTask(3)
            

            
init_information = "Chelsea;Everton;2;0;\nLiverpool;Arsenal;4;0;\nCrystal Palace;Swansea City;0;2;\n\
Newcastle United;West Ham United;3;0;\nManchester United;Leicester City;2;0;\nManchester City;Everton;1;1;\n\
Swansea City;Manchester United;0;4;\nLiverpool;Crystal Palace;1;0;"

def view_file():
    with open('results.txt', 'r') as file:
        for line in file:
            print(line)
            
            
def reinit_file():
    with open('results.txt', 'w') as file:
        file.write(init_information)