from capytale.autoeval import Validate, validationclass
import sys


@validationclass 
class ValidationMessage(Validate):
    def __init__(self,val):
        super().__init__()
        self.val = val

    def __call__(self,message):
        if message.upper() == self.val:
            print('Message décodé avec succès !')
            self._success()  # c'est cet appel qui affiche le reste du notebook
        else:
            print("Désolé ! Le message n'est pas correct !", file=sys.stderr)

validation_message = ValidationMessage('TURING')