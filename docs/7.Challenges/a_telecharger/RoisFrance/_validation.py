from capytale.autoeval import ValidateFunctionPretty

valeurs = [123, 481, 500, 740, 760, 843, 990, 1132, 1300, 1316, 1483, 1530, 1589, 1605, 1650, 1750, 1789, 1810]
valeurs_cibles = ['date non gérée', ['Childéric I', 'Clovis I'], ['Clovis I'], ['interrègne'], ['Pépin le Bref'],
                  ['interrègne', 'Charles II'], ['Hugues Capet'], ['Louis VI'], ['Philippe IV'], 
                  ['Louis X', 'Jean I', 'Philippe V'], ['Louis XI', 'Charles VIII'], ['François I'], ['Henri III', 'Henri IV'], 
                  ['Henri IV'], ['Louis XIV'], ['Louis XV'], ['Louis XVI'], 'date non gérée']

test_a_régné = ValidateFunctionPretty('a_régné', test_values = valeurs, target_values = valeurs_cibles)