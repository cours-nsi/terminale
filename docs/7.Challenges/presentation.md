---
author: à compléter
title: Introduction
---

!!! abstract "Présentation générale"

    On propose dans cette partie divers challenges en algorithmique et en programmation Python et/ou Javascript au format notebook séquencé.

    De nombreux autres challenges sont également disponibles sur ce <a href='https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/presentation/'>site</a> entièrement dédié à la cybersécurité. Ceux-ci sont regroupés en huit catégories :

     - algorithmique
     - cryptographie
     - OSINT
     - programmation Python/Scratch
     - réseaux
     - stéganographie
     - web (client)
     - web (serveur)