---
author: à compléter
title: Challenges
---

## Algorithmique et programmation

### Algorithmique pure


??? note "Art Expo"

    Aidez le galeriste à accéder aux douze tableaux les plus célèbres du monde avant l’inauguration de l’exposition Expo Art (d'après une activité de <a href='https://www.101computing.net/'>101computing.net</a>).

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Art Expo
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Art Expo_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/ArtExpo){ .md-button target="_blank" rel="noopener" }


??? note "Blackbeard’s Hidden Treasures"

    Découvrez les douze lieux où le célèbre pirate Barbe-Noire a caché son trésor (en anglais, d'après une activité de <a href='https://www.101computing.net/'>101computing.net</a>).

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Blackbeard’s Hidden Treasures
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Blackbeard’s Hidden Treasures_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/BlackbeardsHiddenTreasures){ .md-button target="_blank" rel="noopener" }


### Programmation Javascript

??? note "Connexion administrateur"

    Faites vous passer pour l'administrateur d'un système et connectez-vous sur son espace web dédié afin de récupérer son flag.

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Connexion administrateur
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Connexion administrateur_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/ConnexionAdministrateur){ .md-button target="_blank" rel="noopener" }


### Programmation Python


??? note "Blackbeard’s Treasure Map"

    Partez à la recherche du trésor du célèbre pirate Barbe-Noire (en anglais, d'après une activité de <a href='https://www.101computing.net/'>101computing.net</a>).

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Blackbeard’s Treasure Map
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Blackbeard’s Treasure Map_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/BlackbeardsTreasureMap){ .md-button target="_blank" rel="noopener" }


??? note "Football Premier League"

    Suivez d'au plus près le championnat de football de Premier League (en anglais, d'après une activité de <a href='https://www.101computing.net/'>101computing.net</a>).

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Football Premier League
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Football Premier League_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/FootballPremierLeague){ .md-button target="_blank" rel="noopener" }


??? note "Rois de France"

    Partez à la découverte de l'Histoire de France.
    </li>   

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Rois de France
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Rois de France_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/RoisFrance){ .md-button target="_blank" rel="noopener" }



## Cryptographie


??? note "Cryptage renforcé"

    Décodez le message secret caché à l'intérieur d'un dossier hautement crypté.

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Cryptage renforcé
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Cryptage renforcé_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/CryptageRenforcé){ .md-button target="_blank" rel="noopener" }


??? note "Dossier protégé"

    Décodez un message caché à l'intérieur d'un dossier protégé par un mot de passe.

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Dossier protégé
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Dossier protégé_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/DossierProtégé){ .md-button target="_blank" rel="noopener" }


## OSINT


??? note "Espionnage"

    Exécutez une mission secrète à haut risque (d'après Passe ton hack d'abord).

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Challenge Espionnage
    ```

    - **Accès sans Capytale :** télécharger le challenge à l'aide du lien ci-dessous

    [_Espionnage_](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/7.Challenges/a_telecharger/Espionnage){ .md-button target="_blank" rel="noopener" }
