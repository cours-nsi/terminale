---
author: à compléter
title: Principes et architectures
---

# Principes et architectures

![](../images/bd.png){: .center }


!!! tip "Florence Sedes"

    ![](../images/florenceS.png){: .center }

    **Florence Sedes** est professeur d'informatique à l'Université des Sciences Toulouse 3, dans le domaine de recherche de la science des données. Active dans la recherche sur les bases de données et les systèmes d'information depuis mon doctorat en 1987. Publication de plus d'une centaine d'articles, de livres et de chapitres de livres depuis 2000. Elle a dirigé des projets internationaux, européens et nationaux sur la confidentialité et la gestion des (méta)données personnelles, la vidéosurveillance et la médecine légale, l'IoT et la sécurité, les données géospatiales et intérieures/extérieures, et les réseaux sociaux, avec des applications via l'apprentissage profond/machine pour l'alerte, le spam. et détection de rumeurs, émotion sociale et interaction.
	
	[Pour en savoir plus](https://www.irit.fr/~Florence.Sedes/)

## Intentions

!!! abstract "Introduction"

    Le développement des traitements informatiques nécessite la manipulation de données de plus en plus nombreuses. Leur organisation et leur stockage constituent donc un enjeu essentiel de performance. Le recours aux bases de données relationnelles est aujourd’hui une solution très répandue.

    Ces bases de données permettent d’organiser, de stocker, de mettre à jour et d’interroger des données structurées volumineuses utilisées simultanément par différents programmes ou différents utilisateurs, ce qui était impossible avec les représentations tabulaires étudiées en classe de Première.

!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un débat préalable pour faire émarger les idées générales sur les bases de données : pourquoi ? modèles d'architecture ? Service rendus ?
    2. un point de cours pour fixer les idées qui ont pu émerger lors de ce débat.

## Cours

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_1/Principes_architecture_BD_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

_Il n'y a pas de ressources associées à ce chapitre._