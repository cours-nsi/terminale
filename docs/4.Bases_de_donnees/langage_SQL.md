---
author: à compléter
title: Introduction au langage SQL
---

# Introduction au langage SQL



## Intentions

!!! abstract "Introduction"

    Le langage SQL (Structured Query Language) est l'un des langages possibles pour effectuer des requêtes d’interrogation et de mise à jour dans les bases de données relationnelles.

    Le but de ce chapitre est de présenter quelques notions de ce langage.

!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un notebook permettant d'introduire les mots-clefs et requêtes de base du langage SQL ;
    2. un point de cours pour synthétiser le travail des élèves ;
    3. des exercices d'entraînement &laquo; à la main &raquo; pour manipuler quelques requêtes SQL.

## Cours

??? note "Activité d'introduction"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 4 Chapitre 3 Langage SQL introduction
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/SynthèseIntroSQL.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/Langage_SQL_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/4.Bases_de_donnees/a_telecharger/chapitre_3/notebooks){ .md-button target="_blank" rel="noopener" }