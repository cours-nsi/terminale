---
author: à compléter
title: Introduction
---

!!! abstract "Présentation générale"

    Le développement des traitements informatiques nécessite la manipulation de données de plus en plus nombreuses. Leur organisation et leur stockage constituent un enjeu essentiel de performance.
    
    Le recours aux bases de données relationnelles est aujourd’hui une solution très répandue. Ces bases de données permettent d’organiser, de stocker, de mettre à jour et d’interroger des données structurées volumineuses utilisées simultanément par différents programmes ou différents utilisateurs. Cela est impossible avec les représentations tabulaires étudiées en classe de Première.
    
    Des systèmes de gestion de bases de données (SGBD) de très grande taille (de l’ordre du pétaoctet) sont au centre de nombreux dispositifs de collecte, de stockage et de production d’informations. L’accès aux données d’une base de données relationnelle s’effectue grâce à des requêtes d’interrogation et de mise à jour qui peuvent par exemple être rédigées dans le langage SQL (Structured Query Language). Les traitements peuvent conjuguer le recours au langage SQL et à un langage de programmation.




!!! info "Points abordés"

    1. Principes et architectures des bases de données :
        - grands principes d'une base de données : abstraction, indépendance et universalité ;
        - modèles d'architecture ;
        - services rendus : persistance des données, gestion des accès concurrents, efficacité de traitement des requêtes, sécurisation des accès?

    2. Découverte des bases de données relationnelles :
        - relation : nom, attribut, domaine, individu, instance ;
        - contraintes d'intégrité : contrainte de domaine, contrainte d'unicité et clef primaire, contrainte d'inclusion et clef étangère ;
        - schéma relationnel.

    3. Introduction au langage SQL :
        - mots-clefs du langage SQL ;
        - comprendre une requête ;
        - écrire une requête.

