---
author: à compléter
title: Découverte des bases de données relationnelles
---

# Découverte des bases de données relationnelles



## Intentions

!!! abstract "Introduction"

    Dans le chapitre précédent sur les bases de données, nous nous sommes intéressés à des questions générales sur les bases de données : pourquoi en a-t-on besoin ? sur quels modèles d'architectures sont-elles créées ? quels services peuvent-elles rendre ? etc.
    
    Dans ce chapitre, nous nous intéressons plus précisément aux bases de données relationnelles et à l'organisation des leurs éléments.

!!! info "Déroulé"

    On propose le déroulé suivant :

    1. une activité d'introduction pour comprendre en quoi une base de données relationnelles est plus intéressante qu'un simple tableau pour gérer des données multiples ;
    2. un point de cours pour introduire le vocabulaire des bases de données relationnelles (relation, attribut, domaine, schéma, individu, instance), ainsi les différentes contraintes d'intégrité (domaine, unicité, inclusion) ;
    3. des exercices d'entraînement pour manipuler les différents concepts vus dans le cours.

## Cours

??? note "Activité d'introduction"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/Decouverte_BDR_introduction.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/Decouverte_BDR_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/Decouverte_BDR_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

_Il n'y a pas de ressources associées à ce chapitre._