---
author: à compléter
title: Algorithmes de Dijkstra, Prim et Kruskal
---

# Algorithmes de Dijkstra, Prim et Kruskal



## Intentions

!!! abstract "Introduction"

    Dans ce chapitre, on présente deux grandes applications pratiques des graphes :

     - le problème du plus court chemin dans un graphe connexe valué simple avec l'algortihme de Dijkstra ;
     - la détermination d'un arbre couvrant minimal dans un graphe connexe non orienté valué et simple avec les algorithmes de Prim et Kruskal.



!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif présentant ces deux problèmes et les illustrant sur des exemples simples &laquo; à la main &raquo; ;
    2. des exercices d'entraînement pour manipuler ces différents algorithmes sur des exemples simples.

## Cours



??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_5/DPK_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_5/DPK_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

_Il n'y a pas de ressources associées à ce chapitre._