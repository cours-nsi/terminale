---
author: à compléter
title: Parcours de graphes
---

# Parcours de graphes



## Intentions

!!! abstract "Introduction"

    Beaucoup de problèmes sur les graphes nécessitent que l’on parcourt de façon systématique les arêtes/arcs du graphe afin d’en visiter les sommets pour mettre en évidence plusieurs caractéristiques structurelles (sommets accessibles depuis un sommet particulier, chemins, connexité, etc.).
    
    Dans ce chapitre, nous nous intéressons aux deux principales stratégies élémentaires d’exploration : le _parcours en largeur_ et le _parcours en profondeur_.



!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif présentant le parcours en largeur ;
    2. des exercices d'entraînement (sur papier et sur notebook) pour manipuler le parcours en largeur ;
    3. un point de cours interactif présentant le parcours en profondeur ;
    4. des exercices d'entraînement (sur papier et sur notebook) pour manipuler le parcours en profondeur.

## Cours



??? note "Cours (avec les deux parcours)"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/Parcours_graphes_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices (avec les deux parcours)"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/Parcours_graphes_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

    <br>

    Pour les exercice 2 et 4, on utilisera l'une des deux méthodes ci-dessous :

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper les phrases ci-dessous :

    ```python
    NSI Terminale Partie 3 Chapitre 3 Parcours graphes largeur

    NSI Terminale Partie 3 Chapitre 3 Parcours graphes profondeur
    ```

    - **Accès sans Capytale :** télécharger les notebooks dans la rubrique Ressources ci-dessous

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/3.Graphes_et_arbres/a_telecharger/chapitre_3/notebooks){ .md-button target="_blank" rel="noopener" }