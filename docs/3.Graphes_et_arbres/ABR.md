---
author: à compléter
title: Arbres binaires de recherche
---

# Arbres binaires de recherche



## Intentions

!!! abstract "Introduction"

    Dans ce chapitre, nous nous intéressons à une classe particulière d’arbres binaires : les _arbres binaires de recherche_ appelés également les _ABR_.
    
    Ceux-ci définissent des structures de données qui ont pour structure logique un arbre binaire et qui peuvent supporter des opérations courantes sur des ensembles dynamiques ; par exemple : rechercher, minimum, maximum, prédécesseur, successeur, ajouter, supprimer, etc.
    
    Ces arbres sont fondamentaux dans beaucoup de domaines : gestion des fichiers sur un disque dur, etc.



!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif présentant quelques généralités sur la structure des arbres binaires de recherche, ainsi que des algorithmes de recherche et d'ajout de clés ;
    2. des exercices d'entraînement pour manipuler ces différentes notions ;
    3. un notebook pour implémenter des arbres binaires de recherche.

## Cours



??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_7/ABR_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_7/ABR_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Notebook d'implémentation"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 3 Chapitre 7 Implémentation ABR
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/3.Graphes_et_arbres/a_telecharger/chapitre_7/notebook){ .md-button target="_blank" rel="noopener" }