---
author: à compléter
title: Représentation des graphes
---

# Représentation des graphes



## Intentions

!!! abstract "Introduction"

    Dans ce chapitre, on s’intéresse à la représentation des graphes dans un ordinateur. Il en existe plusieurs possibles. Nous étudions ici les deux plus courantes : celle par _matrice d’adjacence_ et celle par _listes d’adjacences_.
    
    Le choix de la représentation dépend bien sûr de la nature du graphe, des algorithmes que l’on veut utiliser et du langage de programmation utilisé pour l’implémentation.



!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif présentant l'implémentation des graphes par matrice d'adjacence et par listes d'adjacence ;
    2. des exercices d'entraînement (sur papier et sur notebook) pour manipuler ces différentes notions.

## Cours



??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/Representation_graphes_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/Representation_graphes_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

    <br>

    Pour l'exercice 5, on utilisera l'une des deux méthodes ci-dessous :

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 3 Chapitre 2 Représentation graphes
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/3.Graphes_et_arbres/a_telecharger/chapitre_2/notebook){ .md-button target="_blank" rel="noopener" }