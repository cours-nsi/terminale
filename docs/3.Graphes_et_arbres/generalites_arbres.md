---
author: à compléter
title: Généralités sur les arbres
---

# Généralités sur les arbres



## Intentions

!!! abstract "Introduction"

    Les arbres sont des graphes possédant une structure relationnelle particulière appelée _structure hiérarchique_. Ils sont parmi les structures de donnzes les plus importantes et les plus utilisées en informatique car ils interviennent dans de très nombreux problèmes.

    Dans ce chapitre, on présente quelques généralités sur les arbres.



!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif introduisant les notions de base sur les arbres ;
    2. des exercices d'entraînement pour manipuler ces différentes notions.

## Cours



??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_4/Generalites_arbres_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_4/Generalites_arbres_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

_Il n'y a pas de ressources associées à ce chapitre._