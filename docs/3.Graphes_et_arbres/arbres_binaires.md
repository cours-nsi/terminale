---
author: à compléter
title: Arbres binaires
---

# Arbres binaires



## Intentions

!!! abstract "Introduction"

    Dans ce chapitre, nous nous intéressons à une classe particulière d’arbres : les _arbres binaires_.
    
    Ceux-ci ont une structure hiérarchique plus rigide que les arbres généraux vus précédemment, ce qui leur permet d’avoir des propriétés très intéressantes et notamment des algorithmes de traitement efficaces et efficients.

    Ces arbres sont fondamentaux dans beaucoup de domaines : gestion des arbres généalogiques, méthodes de compressions, gestion des fichiers sur un disque dur, etc.



!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours interactif présentant quelques généralités sur les arbres binaires et leur exploration à l'aide des parcours en largeur et en profondeur ;
    2. des exercices d'entraînement pour manipuler ces différentes notions ;
    3. un notebook pour implémenter des arbres binaires.

## Cours



??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_6/Arbres_binaires_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_6/Arbres_binaires_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Notebook d'implémentation"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper la phrase ci-dessous :

    ```python
    NSI Terminale Partie 3 Chapitre 6 Implémentation arbres binaires
    ```

    - **Accès sans Capytale :** télécharger le notebook dans la rubrique Ressources ci-dessous

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/3.Graphes_et_arbres/a_telecharger/chapitre_6/notebook){ .md-button target="_blank" rel="noopener" }