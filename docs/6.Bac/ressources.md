---
author: à compléter
title: Ressources à télécharger
---

# Ressources à télécharger


[Intégralité des épreuves de 2021](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/6.Bac/a_telecharger/Annee_2021){ .md-button target="_blank" rel="noopener" }

[Intégralité des épreuves de 2022](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/6.Bac/a_telecharger/Annee_2022){ .md-button target="_blank" rel="noopener" }

[Intégralité des épreuves de 2023](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/6.Bac/a_telecharger/Annee_2023){ .md-button target="_blank" rel="noopener" }

[Intégralité des épreuves de 2024](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/6.Bac/a_telecharger/Annee_2024){ .md-button target="_blank" rel="noopener" }