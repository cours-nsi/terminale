---
author: à compléter
title: Epreuves écrites
---

# Epreuves écrites

## Thématiques

??? abstract "Thèmes des sujets par années"

    <div class="centre">
	<iframe 
	src="../a_telecharger/Thèmes_sujets_Bac_NSI_écrit.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Epreuves par années

??? abstract "Année 2021"

	??? note "Amérique du nord - mars 2021"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/Amerique_Nord_mars_2021.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - mars 2021 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/Métropole_mars_2021_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - mars 2021 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/Métropole_mars_2021_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Polynésie - mars 2021"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/Polynesie_mars_2021.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Centres étrangers - juin 2021 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/CentresEtrangers_juin_2021_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Centres étrangers - juin 2021 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/CentresEtrangers_juin_2021_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - juin 2021 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/Métropole_juin_2021_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - juin 2021 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/Métropole_juin_2021_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - septembre 2021"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2021/Métropole_septembre_2021.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>


??? abstract "Année 2022"

	??? note "Amérique du nord - mai 2022 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Amerique_Nord_mai_2022_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du nord - mai 2022 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Amerique_Nord_mai_2022_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Asie - mai 2022 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Asie_mai_2022_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Asie - mai 2022 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Asie_mai_2022_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Centres étrangers - mai 2022 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/CentresEtrangers_mai_2022_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Centres étrangers - mai 2022 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/CentresEtrangers_mai_2022_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Mayotte - mai 2022 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Mayotte_mai_2022_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Mayotte - mai 2022 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Mayotte_mai_2022_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - mai 2022 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Métropole_mai_2022_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - mai 2022 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Métropole_mai_2022_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Polynésie - mai 2022"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Polynesie_mai_2022.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du sud - septembre 2022 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Amerique_Sud_septembre_2022_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du sud - septembre 2022 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Amerique_Sud_septembre_2022_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - septembre 2022"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2022/Métropole_septembre_2022.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

??? abstract "Année 2023"

	??? note "Sujet 0 - 2023 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Sujet0_2023_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Sujet 0 - 2023 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Sujet0_2023_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Centres étrangers - mars 2023 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/CentresEtrangers_mars_2023_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Centres étrangers - mars 2023 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/CentresEtrangers_mars_2023_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Polynésie - mars 2023 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Polynesie_mars_2023_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Polynésie - mars 2023 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Polynesie_mars_2023_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - mars 2023 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Métropole_mars_2023_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - mars 2023 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Métropole_mars_2023_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Mayotte - mars 2023 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Mayotte_mars_2023_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Mayotte - mars 2023 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Mayotte_mars_2023_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Liban - mars 2023 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Liban_mars_2023_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Liban - mars 2023 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Liban_mars_2023_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du nord - mars 2023 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Amerique_Nord_mars_2023_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du nord - mars 2023 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Amerique_Nord_mars_2023_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du sud - septembre 2023 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Amerique_Sud_septembre_2023_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du sud - septembre 2023 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2023/Amerique_Sud_septembre_2023_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

??? abstract "Année 2024"

	??? note "Sujet 0 - 2024 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Sujet0_2024_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Sujet 0 - 2024 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Sujet0_2024_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du nord - mai 2024 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Amerique_Nord_mai_2024_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Amérique du nord - mai 2024 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Amerique_Nord_mai_2024_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Centres étrangers - juin 2024 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Centres_Etrangers_juin_2024_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Centres étrangers - juin 2024 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Centres_Etrangers_juin_2024_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Asie - juin 2024 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Asie_juin_2024_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Asie - juin 2024 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Asie_juin_2024_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - juin 2024 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Metropole_juin_2024_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - juin 2024 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Metropole_juin_2024_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Polynésie - juin 2024 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Polynesie_juin_2024_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Polynésie - juin 2024 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Polynesie_juin_2024_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - septembre 2024 - sujet 1"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Metropole_septembre_2024_sujet_1.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

	??? note "Métropole - septembre 2024 - sujet 2"

		<div class="centre">
		<iframe 
		src="../a_telecharger/Annee_2024/Metropole_septembre_2024_sujet_2.pdf"
		width="1000" height="1000" 
		frameborder="0" 
		allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
		</iframe>
		</div>

