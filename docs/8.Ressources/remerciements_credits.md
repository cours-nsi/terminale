---
author: à compléter
title: Remerciements et crédits
---

# Remerciements et crédits





## Remerciements

<p>Ce site n'aurait jamais pu voir le jour sans les personnes à l'origine de ces différentes ressources :
<ul>
<li>Mathilde Boehm (académie de Lyon) ;</li>
<li>Charles Poulmaire (académie de Versailles) ;</li>
<li>Pascal Remy (académie de Versailles).</li>
</ul>
Il n'aurait pas pu aussi voir le jour sans le soutien indéfectible de l'<a href='https://aeif.fr/' target="_blank"><span style='font-style:italic;'>Association des Enseignantes et Enseignants d'Informatique de France (AEIF)</span></a> qui a permis de l'héberger.
<br>
<div style="text-align: center;"> 
<img src = "../aeif.png" alt='image'>
</div> 
<br><br>
Les auteurs de ce site tiennent aussi à remercier les équipes de <a href='https://console.basthon.fr/' target="_blank"><span style='font-style:italic;'>Basthon</span></a> et <a href='https://capytale2.ac-paris.fr/web/c-auth/list' target="_blank"><span style='font-style:italic;'>Capytale</span></a> sans qui les notebooks n'auraient aucun sens, et les équipes de <a href='https://portail.apps.education.fr/' target="_blank"><span style='font-style:italic;'>Apps Education</span></a> pour l'hébergement des différentes ressources téléchargeables. </p>
<br>
<div style="text-align: center;"> 
<img src = "../capytale.png" alt='image'>
</div>
<div style="text-align: center;"> 
<img src = "../cap.jpg" alt='image'>
</div>  
<p style='font-style:italic;'>Avec les nouveautés de cette année comme geogebra et pyxel_studio si on ajoute l'expérimentation avec Vittascience
, ce sont désormais 24 types d'activités qui sont disponibles dans Capytale.</p>
<br>
<div style="text-align: center;"> 
<img src = "../apps.png" alt='image'>
</div> 
<br>
<p style='font-style:italic;'><a href='https://projet.apps.education.fr/' target="_blank">Apps.education.fr</a>
 est une plateforme développée au sein de la Direction du Numérique pour l'Education (DNE), qui propose les outils essentiels du quotidien à l'ensemble des agents de l'Education Nationale. Ce projet offre aux utilisateurs une plateforme de Services Numériques Partagés à l'échelle nationale à laquelle l'agent conserve son accès même en cas de changement d'académie.</p>


## Crédits
<p>L'intégralité des ressources présentes sur ce site (pdf, notebooks, ressources téléchargeables) sont sous licence <a href='https://creativecommons.org/licenses/by-sa/4.0/' target="_blank">CC BY-SA 4.0</a>.
<br><br>
Pour toutes questions relatives à ce site, vous pouvez écrire aux concepteurs <a href='mailto:charles.poulmaire@ac-versailles.fr'>Charles Poulmaire</a> et <a href='mailto:pascal.remy@ac-versailles.fr'>Pascal Remy</a>.
</p>
