---
author: à compléter
title: Sécurisation des communications
---

# Sécurisation des communications



## Intentions

!!! abstract "Introduction"

    La sécurisation des communications consistant à protéger les messages sensibles en les rendant illisibles pour les personnes non autorisées à en connaître le contenu est un problème extrêmement ancien remontant à l’Antiquité. Autrefois réservée généralement aux domaines militaires et diplomatiques, elle est à présent répandue dans de nombreux domaines de la vie courante (mouvements bancaires, dossiers médicaux, dossiers fiscaux, déplacements, achats, appels téléphoniques, courriers électroniques, etc.) afin d’assurer une protection contre les indiscrétions, vérifier l’authenticité des messages, conserver l’intégrité d’un message, etc.
	
	L’ensemble des techniques permettant de sécuriser ces différentes communications, connu sous le nom de _cryptographie_, est en
	perpétuelle évolution. Autrefois réalisée à la main, elle utilise aujourd’hui des algorithmes cryptographiques, c’est-à-dire des procédés mathématiques décrivant toutes les étapes nécessaires à la transformation d’un message, dont les exécutions pratiques sont réalisées par des ordinateurs personnels ou des puces insérées dans un dispositif spécialisé, telle une carte de paiement. La mise au point de tels algorithmes nécessitant des théories mathématiques de pointes, la conception des systèmes informatiques de protection est donc aussi bien une affaire d’informaticiens que de mathématiciens.

!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un point de cours permettant d'introduire quelques repères historiques et quelques éléments de crytpographie symétrique et asymétrique ;
    2. un notebook proposant des implémentations de trois méthodes de chiffrement symétrique : César, Vigenère et XOR ;
	3. un notebook proposant une implémentation de la méthode RSA (chiffrement asymétrique) ;
	4. un point de cours pour introduire le concept de chiffrement hybride, illustré à l'aide des protocoles HTTPS et TLS.

## Cours



??? note "Cours (éléments de cryptographie)"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_4/Securisation_communication_introduction.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    - **Accès via Capytale :** dans la zone `Titre` de la bibliothèque de Capytale, taper les phrases ci-dessous :

    ```python
    NSI Terminale Partie 5 Chapitre 4 Cryptographie symétrique

	NSI Terminale Partie 5 Chapitre 4 Cryptographie asymétrique
    ```

    - **Accès sans Capytale :** télécharger les notebooks dans la rubrique Ressources ci-dessous

??? note "Cours (protocoles HTTPS et TLS)"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_4/Securisation_communication_https.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/5.Architecture_materielle/a_telecharger/chapitre_4/notebooks){ .md-button target="_blank" rel="noopener" }