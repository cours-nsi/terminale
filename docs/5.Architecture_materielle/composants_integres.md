---
author: à compléter
title: Composants intégrés
---

# Composants intégrés



## Intentions

!!! abstract "Introduction"

    Les systèmes sur puce, dont l’avènement a été porté par la réduction de taille des éléments des circuits électroniques, permettent de regrouper dans un seul circuit nombre de fonctions autrefois effectuées par des circuits séparés assemblés sur une carte électronique. De tels systèmes sont conçus et mis au point de façon logicielle et ses briques électroniques sont accessibles par des API, comme pour les bibliothèques logicielles.

	L’objectif de ce chapitre est multiple :
	
	 - connaître quelques composants intégrés d’un système sur puce ;
	 - identifier les principaux composants sur un schéma de circuit ;
	 - comprendre les avantages de leur intégration en terme de vitesse et de consommation.

!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un questionnaire que l'élève complètera en s'aidant du cours proposé ;
    2. un bilan des réponses des élèves par l'enseignant.

## Cours

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_1/Composants_integres_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Questionnaire"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_1/Composants_integres_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

_Il n'y a pas de ressources associées à ce chapitre._