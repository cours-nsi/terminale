---
author: à compléter
title: Gestion des processus
---

# Gestion des processus



## Intentions

!!! abstract "Introduction"

    Un système d’exploitation a deux objectifs principaux :
	
	 - réaliser une couche logicielle placée entre la machine matérielle et les applications par le biais d’une interface, composée d’un ensemble de fonctionnalités qui gèrent elles-mêmes les caractéristiques matérielles ;
	 - la prise en charge de la gestion de plus en plus complexe des ressources et de leur partage : cela concerne principalement le processeur, la mémoire centrale et les périphériques d’entrées-sorties.

	La fonction de gestions des exécutions recouvre principalement deux notions : les _processus_ et l’_ordonnancement_.

!!! info "Déroulé"

    On propose le déroulé suivant :

    1. un cours interactif permettant de rappeler les grands principes de base des systèmes d'exploitation et d'introduire les notions clés de processus et d'ordonnancement, ainsi que la notion d'interblocage ;
    2. des exercices d'entraînement pour manipuler ces différentes notions.

## Cours

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/Gestion_processus_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_2/Gestion_processus_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

_Il n'y a pas de ressources associées à ce chapitre._