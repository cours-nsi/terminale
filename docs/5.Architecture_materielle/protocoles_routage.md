---
author: à compléter
title: Protocoles de routage
---

# Protocoles de routage



## Intentions

!!! abstract "Introduction"

    Lorsqu’un routeur reçoit un paquet, il regarde l’adresse IP du destinataire. Si cette adresse n’appartient pas au même réseau que l’expéditeur, alors le processus de routage doit être enclenché pour rechercher le point de passage (routeur) pour atteindre le réseau de l’ordinateur voulu. Chaque routeur examine alors sa table de routage pour déterminer le routeur suivant, c’est-à-dire l’adresse d’un routeur immédiatement voisin et situé sur la route vers la destination.

	Suivant le protocole de routage utilisé, les tables de routage sont différentes et les chemins empruntés par les paquets peuvent également varier.

!!! info "Déroulé"

    On propose le déroulé suivant :

    1. une activité d'introduction pour rappeler et compléter les notions vues en Première sur les réseaux (le questionnaire sur les vidéos peut être donné à faire à la maison avant de débuter ce chapitre) ;
    2. un point de cours pour introduire les notions de tables de routage et les protocoles RIP et OSPF ;
	3. des exercices d'entraînement pour manipuler ces deux protocoles.

## Cours

??? note "Activité d'introduction"

	**Remarque : des fichiers Filius sont à télécharger dans l'onglet Ressources ci-dessous.**

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/Protocoles_routage_introduction.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Cours"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/Protocoles_routage_cours.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Exercices"

    <div class="centre">
	<iframe 
	src="../a_telecharger/chapitre_3/Protocoles_routage_cours_exercices.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

## Ressources

[Téléchargement des ressources](https://forge.apps.education.fr/cours-nsi/terminale/-/tree/main/docs/5.Architecture_materielle/a_telecharger/chapitre_3/Fichiers_protocoles_routage_introduction){ .md-button target="_blank" rel="noopener" }