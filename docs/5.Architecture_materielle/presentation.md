---
author: à compléter
title: Introduction
---

!!! abstract "Présentation générale"

    La réduction de taille des éléments des circuits électroniques a conduit à l’avènement de systèmes sur puce (SoCs pour _Systems on Chips_ en anglais) qui regroupent dans un seul circuit nombre de fonctions autrefois effectuées par des circuits séparés assemblés sur une carte électronique. Un tel système sur puce est conçu et mis au point de façon logicielle, ses briques électroniques sont accessibles par des API, comme pour les bibliothèques logicielles.
    
    Toute machine est dotée d’un système d’exploitation qui a pour fonction de charger les programmes depuis la mémoire de masse et de lancer leur exécution en leur créant des processus, de gérer l’ensemble des ressources, de traiter les interruptions ainsi que les entrées-sorties et enfin d’assurer la sécurité globale du système.
    
    Dans un réseau, les routeurs jouent un rôle essentiel dans la transmission des paquets sur Internet : les paquets sont routés individuellement par des algorithmes. Les pertes logiques peuvent être compensées par des protocoles reposant sur des accusés de réception ou des demandes de renvoi, comme TCP.
    
    La protection des données sensibles échangées est au coeur d’Internet. Les notions de chiffrement et de déchiffrement de paquets pour les communications sécurisées sont explicitées.




!!! info "Points abordés"

    1. Composants intégrés :
        - identification des principaux composants d'un SoC ;
        - avantages et inconvénients d'un SoC par rapport à un système classique ;
        - éléments d'architecture ARM.

    2. Gestion des processus :
        - rappels sur les systèmes d'exploitation ;
        - création et ordonnancement des processus ;
        - mise en évidence du risque d'interblocage.

    3. Protocoles de routage :
        - rappels sur les réseaux ;
        - protocole RIP ;
        - protocole OSPF.

    4. Sécurisation des communications :
        - repères historiques ;
        - éléments de cryptographie symétrique et asymétrique ;
        - sûreté des systèmes de chiffrement ;
        - systèmes de chiffrement hybride ;
        - étude des protocoles HTTPS et TLS.

